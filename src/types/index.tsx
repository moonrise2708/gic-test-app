export interface MyStore {
    'Investment': number;
    'Commitment Date': string;
    'Market Value': number;
}

export interface MainState {
    data: Object[]
}