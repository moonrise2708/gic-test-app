import { combineReducers } from "redux"
import  { mainReducer } from "./main/main.reducer"
import {MainState} from '../types'

export interface ApplicationState {
    main: MainState
}

// Contain all current reducers
export const rootReducer = combineReducers({
    main: mainReducer
})