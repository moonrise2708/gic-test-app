import { SET_STORE } from "../../constants"
import { MainActionsTypes } from '../../actions/table.types'
import { MainState } from "../../types"

const initState: MainState = {
    data: []
}

export function mainReducer(state = initState, action: MainActionsTypes): MainState {
    switch(action.type) {
        case SET_STORE: {
            return {
                ...state,
                data: action.payload
            }
        }
        default: 
            return state     
    }
}

