import React from 'react'
import {shallow, mount} from 'enzyme'
import Table from "./index"

describe('Table', () => {
    const data = [{ 'Investment': 100, 'Commitment Date': "22/10/2010", 'Market Value': 100 }]
    const wrapper = shallow(<Table data={data}/>);
    it('render table correctly', () => {
        expect(wrapper).toMatchSnapshot();
    })

    it('table column should draggable', () => {
        const wrapper = mount(<Table data={data}/>);
        expect(wrapper.find('th[table-h-id="Investment"]').getDOMNode().getAttribute('draggable')).toBeTruthy();
    })

    it('simulate table column start dragging', () => {
        const onDragStart = jest.fn()
        wrapper.find('th[table-h-id="Investment"]').simulate("dragstart")
    })

    it('simulate table column dragging over', () => {
        wrapper.find('th[table-h-id="Investment"]').simulate("dragover")
    })

    it('simulate table column dragging enter', () => {
        wrapper.find('th[table-h-id="Investment"]').simulate("dragenter")
    })
})