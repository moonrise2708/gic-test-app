import * as React from "react";
import Table from "react-bootstrap/Table";
import Spinner from "react-bootstrap/Spinner";
import LazyLoad from "react-lazyload";
import { MDBIcon } from "mdbreact";

interface State {
  tableColumns: string[];
  dragOverCol: string;
  currentSort: string;
  currentSortColumn: string;
}

interface PropsFromState {
  data: Object[];
}

export type AllProps = PropsFromState;

class TableComponent extends React.Component<AllProps, State> {
  constructor(props: AllProps) {
    super(props);
    this.state = {
      dragOverCol: "",
      tableColumns: [],
      currentSort: "down",
      currentSortColumn: "Investment"
    };
  }

  componentDidMount() {
    if (this.props.data.length > 0) {
      this.setState({ tableColumns: Object.keys(this.props.data[0]) });
    }
  }

  renderLoading = () => {
    return (
      <Spinner animation="border" role="status">
        <span className="sr-only">Loading...</span>
      </Spinner>
    );
  };

  onSortChange = (sortColumn: string) => {
    const { currentSort } = this.state;
    let nextSort: string = "down";

    if (currentSort === "down") nextSort = "up";
    else if (currentSort === "up") nextSort = "down";

    this.setState({
      currentSort: nextSort,
      currentSortColumn: sortColumn
    });
  };

  handleSort = (tblRowA: any, tblRowB: any): number => {
    const { currentSort, currentSortColumn } = this.state;
    if (currentSortColumn === "Commitment Date") {
      switch (currentSort) {
        case "up":
          return tblRowB.localeCompare(tblRowA);
        case "down":
          return tblRowA.localeCompare(tblRowB);
        default:
          return tblRowA;
      }
    }
    switch (currentSort) {
      case "up":
        return tblRowA - tblRowB;
      case "down":
        return tblRowB - tblRowA;
      default:
        return tblRowA;
    }
  };

  handleDragStart = (e: React.DragEvent<HTMLDivElement>): void => {
    if (e) {
      const { id } = e.target as HTMLDivElement;
      const { tableColumns } = this.state;
      const idx = tableColumns.indexOf(id);
      e.dataTransfer.setData("colIdx", idx.toString());
    }
  };

  handleDragOver = (e: React.DragEvent<HTMLDivElement>) => {
    if (e) e.preventDefault();
  };

  // handleDragEnter = (e: React.DragEvent<HTMLDivElement>) => {
  //   if (e) {
  //     const { id } = e.target as HTMLDivElement;
  //     this.setState({ dragOverCol: id });
  //   }
  // };

  handleOnDrop = (e: any) => {
    const { tableColumns } = this.state;
    const { id } = e.target as HTMLDivElement;
    const droppedColIdx = tableColumns.indexOf(id);
    const draggedColIdx = e.dataTransfer.getData("colIdx");
    const tempCols = [...tableColumns];

    tempCols[draggedColIdx] = tableColumns[droppedColIdx];
    tempCols[droppedColIdx] = tableColumns[draggedColIdx];
    this.setState({ tableColumns: tempCols, dragOverCol: "" });
  };

  render() {
    const { tableColumns, currentSortColumn } = this.state;
    const { data } = this.props;
    return (
      <Table striped bordered hover>
        <thead>
          <tr>
            {tableColumns.map(col => (
              <th
                table-h-id={col}
                id={col}
                key={col}
                draggable
                onDragStart={this.handleDragStart}
                onDragOver={this.handleDragOver}
                onDrop={this.handleOnDrop}
                //onDragEnter={this.handleDragEnter}
                //dragOver={col.id === dragOverCol}
              >
                {col}
                
                <MDBIcon
                  style={{ marginLeft: 5 }}
                  icon="sort"
                  onClick={() => this.onSortChange(col)}
                />
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          <LazyLoad>
            {data
              .sort((rowA: any, rowB: any) =>
                this.handleSort(
                  rowA[currentSortColumn],
                  rowB[currentSortColumn]
                )
              )
              .map((row: any, index) => (
                <tr>
                  {Object.entries(row).map(([k, v], idx) => (
                    <td
                      key={`tblRowData${idx}`}
                      //dragOver={tableColumns[idx].id === dragOverCol}
                    >
                      {row[tableColumns[idx]]}
                    </td>
                  ))}
                </tr>
              ))}
          </LazyLoad>
        </tbody>
      </Table>
    );
  }
}

export default TableComponent;
