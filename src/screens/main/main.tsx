import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import * as MainActions from "../../actions/table.action";
import Table from "./components/table";
import { ApplicationState } from "../../reducers";
import { MyStore } from "../../types";

interface PropsFromState {
  data: Object[];
}

interface PropsFromDispatch {
  getData: typeof MainActions.getStoreData;
}

export type AllProps = PropsFromState & PropsFromDispatch;

class MainScreen extends React.Component<AllProps> {
  componentDidMount() {
    this.props.getData();
  }

  render() {
    return this.props.data.length > 0 ? (
      <Table data={this.props.data} />
    ) : (
      <div />
    );
  }
}

export const mapStateToProps = ({ main }: ApplicationState) => {
  return {
    data: main.data
  };
};

export const mapDispatchToProps = (dispatch: Dispatch) => ({
  getData: () => dispatch(MainActions.getStoreData())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainScreen);
