import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { Provider } from "react-redux";
import { ApplicationState } from "./reducers";
import configStore from "./configs/store";
it("renders without crashing", () => {
  const initSate: ApplicationState = {
    main: {
      data: []
    }
  };

  const { store } = configStore(initSate);
  const div = document.createElement("div");
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
