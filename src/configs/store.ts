import { applyMiddleware, createStore } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"
import createSagaMiddleware from "redux-saga"
import { fromJS } from "immutable"
import {ApplicationState, rootReducer} from "../reducers"
import {rootSaga} from "../saga"

export default function configStore(initState: ApplicationState) {
    const composeEnhancers = composeWithDevTools({})
    const sagaMiddleware = createSagaMiddleware()
    const store = createStore(rootReducer, fromJS(initState), composeEnhancers(applyMiddleware(sagaMiddleware)))
    sagaMiddleware.run(rootSaga);
    return { store }
}