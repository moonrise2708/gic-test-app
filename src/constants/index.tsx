export const GET_STORE = 'GET STORE';
export type GET_STORE = typeof GET_STORE;
export const SET_STORE = 'SET STORE';
export type SET_STORE = typeof SET_STORE;