import * as constants from '../constants';

//define action interfaces
interface GetStoreData {
    type: constants.GET_STORE;
}
interface SetStoreData {
    type: constants.SET_STORE;
    payload: Object[];
}

export type MainActionsTypes = GetStoreData | SetStoreData

