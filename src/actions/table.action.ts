import * as constants from '../constants';
import { MainActionsTypes } from './table.types'

//define actions
export function getStoreData(): MainActionsTypes {
    return {
        type: constants.GET_STORE
    }
};

export function setStoreData(storeData: Object[]): MainActionsTypes {
    return {
        type: constants.SET_STORE,
        payload: storeData
    }
};
