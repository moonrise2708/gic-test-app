import moment from 'moment'
import { put, takeLatest } from 'redux-saga/effects'
import faker from 'faker'
import { GET_STORE } from '../../constants'
import { setStoreData } from "../../actions/table.action"

export default function* watchSources() {
    yield takeLatest(GET_STORE, getData)
}

export function* getData() {
    try {
        let data: Object[] = [];
        for (let i = 0; i < 10000; i++) {
            const investment = faker.random.number();
            const commitmentDate = moment(faker.date.recent()).format("DD/MM/YYYY");
            const marketValue = faker.random.number();
            let singleData: Object = { 'Investment': investment, 'Commitment Date': commitmentDate, "Market Value": marketValue }
            data.push(singleData)
        }
        yield put(setStoreData(data))
    } catch (error) {

    }
}