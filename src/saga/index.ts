import { all } from "redux-saga/effects"
import mainWatchSource from "./main/main.saga"

export function* rootSaga() {
    yield all([
        mainWatchSource()
    ])
}